import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController _userController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  String? _usernameError;
  String? _passwordError;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          constraints: const BoxConstraints.expand(),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                  child: TextField(
                    controller: _userController,
                    style: TextStyle(fontSize: 18, color: Colors.black),
                    decoration: InputDecoration(
                      labelText: "USERNAME",
                      errorText: _usernameError,
                      labelStyle: TextStyle(color: Color(0xff888888), fontSize: 15)),
                  )
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                  child: TextField(
                    controller: _passController,
                    style: TextStyle(fontSize: 18, color: Colors.black),
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: "PASSWORD",
                        errorText: _passwordError,
                        labelStyle: TextStyle(color: Color(0xff888888), fontSize: 15)),
                  )
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: SizedBox(
                  width: double.infinity,
                  height: 56,
                  child: RaisedButton(
                      onPressed: onSignInClicked,
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadiusDirectional.all(Radius.circular(8))),
                      child: Text("SIGN IN", style: TextStyle(color: Colors.white, fontSize: 16)),
                  )
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onSignInClicked() {
    setState(() {
        if (!_userController.text.contains("@")){
          _usernameError = "Username must contain @";
        }

        if (_passController.text.length < 8){
          _passwordError = "Password at least 8 characters";
        }

        if (!_passController.text.contains(new RegExp(r'[a-z]'))){
          _passwordError = "Password contains at least one lowercase letter";
        }

        if (!_passController.text.contains(new RegExp(r'[A-Z]'))){
          _passwordError = "Password contains at least one uppercase letter";
        }

        if (!_passController.text.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
          _passwordError = "Password contains at least 1 special character";
        }
    });
  }


}


